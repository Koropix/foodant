INSERT INTO
    item(id, price, name, short_description, img_url, description)
VALUES
    (1, 22.50, 'Pizza Margherita', 'Włoska pizza na cienkim cieście z serem i sosem pomidorowym',
    '/img/pizza-margherita.jpg',
    'Pizza Margherita to klasyka włoskiej kuchni. W naszym wydaniu serwujemy pizzę na cienkim cieście z ' ||
    'ręcznie przygotowanym sosem pomidorowym i oryginalnym serem mozarella.'),
    (2, 25.99, 'Pizza Pepperoni', 'Włoska pizza na cienkim cieście z serem, sosem pomidorowym i kiełbasą pepperoni',
     '/img/pizza-pepperoni.jpg',
     'Pizza Pepperoni to w Polsce i na talerzu Zosi klasyka. U nas zjesz ją z pikantną i aromatyczną kiełbasą pepperoni, ' ||
     'ręcznie przygotowanym sosem pomidorowym i oryginalnym serem mozarella.'),
    (3, 25.99, 'Pizza Capriciosa', 'Pizza na cienkim cieście z serem, sosem pomidorowym i pieczarkami',
    '/img/pizza-capriciosa.jpg',
    'Pizza Capriciosa to po Marghericie najpopularniejsza wersja tej smacznej włoskiej potrawy. Oprócz naszego ' ||
    'domowego sosu pomidorowego i oryginalnej włoskiej mozarelli znajdziesz tutaj także pieczarki i wyśmienitą sznkę.'),
    (4, 32.99, 'Spaghetti Bolognese', 'Robiona na miejscu pasta z sosem pomidorowym i wołowiną',
     '/img/spaghetti.jpg',
    'Idealna propozycja dla każdego miłośnika pasty. Spaghetti bolognese w naszym wykonaniu to ręcznie przygotowany ' ||
    'makaron z sosem pomidorowym oraz wysokiej jakości wołowiną. Do przygotowania sosu wykorzystujemy pomidory z ' ||
    'lokalnych upraw oraz świeże zioła.'),
    (5, 18.99, 'Panna Cotta', 'Klasyczny włoski deser o smaku śmietankowym z polewą z truskawek',
     '/img/panna-cotta.jpg',
    'Masz ochodę na coś słodkiego? Ta propozycja to nasza interpretacja jednego z najbardziej rozpoznawalnych włoskich' ||
    'deserów. Przygotowujemy go ze śmietanki BIO, świeżego mleka i prawdziwej wanilii. Na życzenie deser podajemy ' ||
    'w alternatywnej wersji z sosem czekoladowym.');

INSERT INTO
    client_order(id, address, telephone, status)
VALUES
    (1, 'Zakrzewskiego 23/5, 05-225 Warszawa', '888 777 666', 'NEW'),
    (2, 'Kościuszki 13, 02-316 Warszawa', '767 454 989', 'NEW'),
    (3, 'Krakowska 88/16, 01-515 Warszawa', '666 234 123', 'IN_PROGRESS'),
    (4, 'Centralna 8/12, 05-100 Warszawa', '598 787 999', 'IN_PROGRESS'),
    (5, 'Dworcowa 33, 01-200 Warszawa', '600 700 900', 'COMPLETE'),
    (6, 'Krucza 66/4, 03-300 Warszawa', '696 787 898', 'COMPLETE');

INSERT INTO
    order_item (order_id, item_id)
VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (2, 3),
    (2, 4),
    (3, 1),
    (3, 1),
    (3, 1),
    (4, 1),
    (4, 2),
    (5, 3),
    (5, 3),
    (5, 4),
    (6, 4),
    (6, 4);
