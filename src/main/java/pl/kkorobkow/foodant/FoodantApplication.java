package pl.kkorobkow.foodant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodantApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodantApplication.class, args);
    }

}
