package pl.kkorobkow.foodant.item;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Long> {
    public Optional<Item> findItemByNameIgnoreCase(String name);
}