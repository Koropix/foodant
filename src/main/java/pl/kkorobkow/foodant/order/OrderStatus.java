package pl.kkorobkow.foodant.order;

public enum OrderStatus {
    NEW,
    IN_PROGRESS,
    COMPLETE;

    static OrderStatus nextStatus(OrderStatus status) {
        return switch (status) {
            case NEW -> IN_PROGRESS;
            case IN_PROGRESS -> COMPLETE;
            default -> throw new IllegalArgumentException("There is no next status for provided value");
        };
    }
}