package pl.kkorobkow.foodant.order;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByOrderByStatusDesc();
    List<Order> findAllByStatus(OrderStatus status);
}