package pl.kkorobkow.foodant.order;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.kkorobkow.foodant.common.Message;
import pl.kkorobkow.foodant.item.Item;

import java.util.List;
import java.util.Optional;

@Controller
public class OrderPanelController {
    private OrderRepository orderRepository;

    public OrderPanelController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @GetMapping("/panel/zamowienia")
    public String getOrders(@RequestParam(required = false) OrderStatus status, Model model) {
        List<Order> orders;
        if (status == null)
            orders = orderRepository.findAllByOrderByStatusDesc();
        else
            orders = orderRepository.findAllByStatus(status);
        model.addAttribute("orders", orders);
        return "panel/orders";
    }

    @GetMapping("/panel/zamowienia/{id}")
    public String singleOrderDetails(@PathVariable Long id, Model model) {
        Optional<Order> order = orderRepository.findById(id);
        return assignOptionalOrderToModel(order, model);
    }

    @PostMapping("/panel/zamowienia/{id}")
    public String changeOrderStatus(@PathVariable Long id, Model model) {
        Optional<Order> order = orderRepository.findById(id);
        order.ifPresent(o -> {
            o.setStatus(OrderStatus.nextStatus(o.getStatus()));
            orderRepository.save(o);
        });
        return assignOptionalOrderToModel(order, model);
    }


    @PostMapping("/panel/zamowienia/usun/{id}")
    public String removeSingleOrder(@PathVariable Long id, Model model) {
        Optional<Order> order = orderRepository.findById(id);
        order.ifPresent(orderRepository::delete);
        if (order.isPresent()) {
            model.addAttribute("message", new Message("Zamówienie usunięto", "Zamówienie " +
                    "nr " + id + " zostało usunięte z systemu"));
        } else {
            model.addAttribute("message", new Message("Błąd", "Zamówienie o nr "
                    + id + "nie istnieje"));
        }
        return "panel/message";
    }

    private String assignOptionalOrderToModel(Optional<Order> order, Model model) {
        return order.map( o -> assignOrderToModel(o, model))
                .orElse("redirect:/panel/zamowienia");
    }

    private String assignOrderToModel(Order order, Model model) {
        model.addAttribute("order", order);
        model.addAttribute("sum", order.getItems().stream()
                .mapToDouble(Item::getPrice).sum());
        return "panel/order";
    }
}
